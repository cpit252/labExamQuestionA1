## Questions

1. This project makes a connection to a remote database. The database connection is poorly implemented in a way that reconnects multiple times to the database to perform any operation. Fix the current implementation, so it connects once to the database and the unit testing can pass.
2. The `Team` class has poor abstraction. Refactor the code.


## Build
```
mvn compile
```

## Test

```
mvn test
```
