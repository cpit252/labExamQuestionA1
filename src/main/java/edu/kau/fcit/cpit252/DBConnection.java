package edu.kau.fcit.cpit252;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection{
    private static Connection connection;

    private  DBConnection instance;

    private DBConnection() throws SQLException {
        final String host = "ec2-52-3-2-245.compute-1.amazonaws.com";
        final String dbName = "d8hb1dojctii1f";
        final String username = "rkjlxoxyotwcua";
        final String password = "0df586cd531aec5bb7510db74aba4be664318b31f405bec68deb5b543a3327f2";
        final String port = "5432";
        final String dsn = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
        Properties props = new Properties();
        props.setProperty("user", username);
        props.setProperty("password", password);
        props.setProperty("ssl","false");
        this.connection = DriverManager.getConnection(dsn, props);
    }

    public Connection getConnection() {
        return this.connection;
    }

    public DBConnection getInstance() throws SQLException {
        return new DBConnection();
    }
}