package edu.kau.fcit.cpit252;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Team {
    private String team;
    private String city;
    private int yearFounded;
    private int championships;

    public Team() {
    }

    public Team(String team, String city, int yearFounded, int championships) {
        this.team = team;
        this.city = city;
        this.yearFounded = yearFounded;
        this.championships = championships;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(int yearFounded) {
        this.yearFounded = yearFounded;
    }

    public int getChampionships() {
        return championships;
    }

    public void setChampionships(int championships) {
        this.championships = championships;
    }

    public List<Team> retrieveTeams(Connection dbConnection){
        try {
            Statement stmt = dbConnection.createStatement();
            String query = "SELECT id, team, loc, founded, championships FROM teams";
            ResultSet rs = stmt.executeQuery(query);
            List<Team> teams = new ArrayList<Team>();
            while(rs.next()){
                Team t = new Team(rs.getString("team"),
                        rs.getString("loc"), rs.getInt("founded"),
                        rs.getInt("championships") );
                teams.add(t);
            }
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toString(){
        return "Team: " + this.team + "\nLocation: " + this.city
                + "\nFounded in:"  + this.yearFounded +
                "\nChampionships: " + this.championships + "\n\n";
    }
}
