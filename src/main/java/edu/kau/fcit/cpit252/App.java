package edu.kau.fcit.cpit252;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        try {
            Connection dbConnection = DBConnection.getInstance().getConnection();
            List<Team> teams = new Team().retrieveTeams(dbConnection);
            for (Team t: teams) {
                System.out.println(t);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
