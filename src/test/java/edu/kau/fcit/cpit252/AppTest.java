package edu.kau.fcit.cpit252;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Should represent the team object as a string
     */
    @Test
    public void shouldToStringWork()
    {
        Team t = new Team("Nuggets", "Denver, Colorado", 1967, 0);
        Assert.assertThat(t.toString(), CoreMatchers.containsString("Denver"));
    }
}
