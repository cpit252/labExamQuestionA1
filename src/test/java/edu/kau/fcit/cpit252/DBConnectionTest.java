package edu.kau.fcit.cpit252;

import junit.framework.TestCase;
import org.junit.Test;

import java.sql.SQLException;

public class DBConnectionTest extends TestCase {

    @Test
    public void testConnection() throws SQLException, InterruptedException {
        DBConnection instance1 = DBConnection.getInstance();
        Thread.sleep(1000);
        DBConnection instance2 = DBConnection.getInstance();;
        assertEquals(instance1, instance2);
        }
}
